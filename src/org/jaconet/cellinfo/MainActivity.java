package org.jaconet.cellinfo;


import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.telephony.CellInfo;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.widget.TextView;

public class MainActivity extends Activity {

   /** Called when the activity is first created. */
   @Override
   public void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_main);
       TextView textGsmCellLocation = (TextView)findViewById(R.id.gsmcelllocation);
       TextView textMCC = (TextView)findViewById(R.id.mcc);
       TextView textMNC = (TextView)findViewById(R.id.mnc);
       TextView textCID = (TextView)findViewById(R.id.cid);


       //retrieve a reference to an instance of TelephonyManager
       TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
       GsmCellLocation cellLocation = (GsmCellLocation)telephonyManager.getCellLocation();

       String networkOperator = telephonyManager.getNetworkOperator();
       String mcc = networkOperator.substring(0, 3);
       String mnc = networkOperator.substring(3);
       textMCC.setText("mcc: " + mcc);
       textMNC.setText("mnc: " + mnc);

       int cid = cellLocation.getCid();
    //   int lac = cellLocation.getLac();
       textGsmCellLocation.setText(cellLocation.toString());
       textCID.setText("gsm cell id: " + String.valueOf(cid));

       TextView Neighboring = (TextView)findViewById(R.id.neighboring);
       List<CellInfo> CellList = telephonyManager.getAllCellInfo();

       String stringNeighboring = "Neighboring List- Lac : Cid : RSSI\n";


       for(int i=0; i < CellList.size(); i++){

    	   stringNeighboring += "\n"+GetCellData(CellList.get(i));
       }
       Neighboring.setText(stringNeighboring);
   }

   String GetCellData (CellInfo data)
   {
	   String retdata="";
	   int Lac,Cid,Mcc=0,Mnc=0,dBm,Psc=0;
	   if (data instanceof CellInfoGsm)
	   {
		   Lac=((CellInfoGsm) data).getCellIdentity().getLac();
		   Cid=((CellInfoGsm) data).getCellIdentity().getCid();
		   Psc=-1;
		   Mcc=((CellInfoGsm) data).getCellIdentity().getMcc();
		   Mnc=((CellInfoGsm) data).getCellIdentity().getMnc();
		   dBm=((CellInfoGsm) data).getCellSignalStrength().getDbm();
		   if (Cid==Integer.MAX_VALUE) retdata=" GSM "+"       "+dBm+ "dBm";
		   else retdata=" GSM "+Lac+" "+Cid+" "+dBm+ "dBm";
	   }
	   else if (data instanceof CellInfoWcdma)
	   {
		   Lac=((CellInfoWcdma) data).getCellIdentity().getLac();
		   Cid=((CellInfoWcdma) data).getCellIdentity().getCid();
		   Psc=((CellInfoWcdma) data).getCellIdentity().getPsc();
		   Mcc=((CellInfoWcdma) data).getCellIdentity().getMcc();
		   Mnc=((CellInfoWcdma) data).getCellIdentity().getMnc();
		   dBm=((CellInfoWcdma) data).getCellSignalStrength().getDbm();
		   if (Cid==Integer.MAX_VALUE) retdata=" UMTS "+Psc+" "+dBm+ "dBm";
		   else retdata=" UMTS "+Lac+" "+Cid+" "+dBm+ "dBm";
	   }
	   else if (data instanceof CellInfoLte)
	   {
		   Lac=((CellInfoLte) data).getCellIdentity().getTac();
		   Cid=((CellInfoLte) data).getCellIdentity().getCi();
		   Psc=((CellInfoLte) data).getCellIdentity().getPci();
		   Mcc=((CellInfoLte) data).getCellIdentity().getMcc();
		   Mnc=((CellInfoLte) data).getCellIdentity().getMnc();
		   dBm=((CellInfoLte) data).getCellSignalStrength().getDbm();
		   retdata=" LTE "+Lac+" "+Cid+" "+" "+Psc+dBm+ "dBm";
	   }
	   if (Mcc!=Integer.MAX_VALUE) retdata=Mcc+" "+Mnc+retdata;
	   else retdata="       "+retdata;
	   return retdata;
   }

}